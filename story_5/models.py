from django.db import models

# Create your models here.
class Schedule(models.Model):
    course = models.CharField(max_length=50, blank=False, default='course')
    lecturer = models.CharField(max_length=50, blank=False, default='lecturer')
    number_of_credits = models.PositiveIntegerField(blank=False, default=0)
    description = models.CharField(max_length=50, blank=False, default='description')
    semester = models.CharField(max_length=100, blank=False, default='semester')
    place = models.CharField(max_length=100, blank=False)
