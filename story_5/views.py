from django.shortcuts import render, redirect
from .models import Schedule
from .forms import ScheduleForm


# Create your views here.
def schedule_form(request):
    if request.method == "POST":
        form = ScheduleForm(request.POST)
        if form.is_valid():
            schedule = Schedule()
            schedule.course = form.cleaned_data['course']
            schedule.lecturer = form.cleaned_data['lecturer']
            schedule.number_of_credits = form.cleaned_data['number_of_credits']
            schedule.description = form.cleaned_data['description']
            schedule.semester = form.cleaned_data['semester']
            schedule.place = form.cleaned_data['place']
            schedule.save()
            return redirect('result/')
        return render(request,'story_5/schedule.html',{'form' : form})
    form = ScheduleForm()
    return render(request,'story_5/schedule.html',{'form' : form})


def schedule_show(request):
    schedule = Schedule.objects.all()
    response = {
        'schedule':schedule, 
    }
    return render(request,'story_5/scheduleresult.html',response)

def schedule_remove(request, id):
    Schedule.objects.filter(id=id).delete()
    schedule = Schedule.objects.all()
    response = {
        'schedule':schedule, 
    }
    return render(request, 'story_5/scheduleresult.html',response)
