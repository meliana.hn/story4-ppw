from django import forms

class ScheduleForm(forms.Form):
    
    course = forms.CharField(
        widget = forms.TextInput(attrs={
        'class': 'form-control',
        'id' : 'control',
        'type' : 'text',
        'placeholder': 'Course',
        'required': True,
        }
    ))
    
    lecturer = forms.CharField(
        widget = forms.TextInput(attrs={
        'class': 'form-control',
        'id' : 'control',
        'type' : 'text',
        'placeholder': 'Lecturer',
        'required': True,
        }
    ))

    number_of_credits = forms.IntegerField(
        min_value=0,
        max_value=10,
        widget = forms.NumberInput(attrs={
        'class': 'form-control',
        'id' : 'control',
        'type' : 'number',
        'placeholder': '0',
        'required': True,
        }
    ))

    description = forms.CharField(
        widget = forms.TextInput(attrs={
        'class': 'form-control',
        'id' : 'control',
        'type' : 'text',
        'placeholder': 'Description',
        'required': True,
        }
    ))
    
    SEMESTER = [
    ('Gasal 2017/2018', 'Gasal 2017/2018'),
    ('Genap 2018/2019', 'Genap 2018/2019'),
    ('Gasal 2019/2020', 'Gasal 2019/2020'),
    ('Genap 2020/2021', 'Genap 2020/2021'),
    ('Gasal 2021/2022', 'Gasal 2021/2022'),
    ('Genap 2022/2023', 'Genap 2022/2023'),
    ]

    semester = forms.CharField(
        widget = forms.Select(attrs={
        'class': 'form-control',
        'id' : 'control',
        'type' : 'text',
        'placeholder': 'Semester',
        'required': True,
        },
        choices=SEMESTER
    ))
    
    place = forms.CharField(
        widget = forms.TextInput(attrs={
        'class': 'form-control',
        'id' : 'control',
        'type' : 'text',
        'placeholder': 'Place',
        'required': True,
        }
    ))
