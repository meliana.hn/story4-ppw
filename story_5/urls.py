from django.urls import path
from . import views

urlpatterns = [
   path('', views.schedule_form, name = 'schedule'),
   path('result/', views.schedule_show, name = 'scheduleresult'),
   path('result/<int:id>', views.schedule_remove, name = 'scheduleremove'),
]
