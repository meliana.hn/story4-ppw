from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def gallery(request):
    return render(request, 'gallery.html')

def organizer(request):
    return render(request, 'organizer.html')